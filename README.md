**Kubernetes学习之路**

# Dokcer 基础

# Kubernetes 基础
1. [Kubernetes 命令](kube-cmd.md)
1. [Gitlab CI](https://www.qikqiak.com/k8s-book/docs/65.Gitlab%20CI.html)

# 参考
1. 和我一步步部署 kubernetes 集群 . https://github.com/opsnull/follow-me-install-kubernetes-cluster

# 书单
+ **Docker 相关**
    1. [Docker 从入门到实践](https://github.com/yeasy/docker_practice) : [下载PDF](https://github.com/hbulpf/MLBooks/blob/master/6_%E4%BA%91%E8%AE%A1%E7%AE%97/docker_practice.pdf)
    
+ **Kubernetes 相关**
    1. [kubernetes-handbook](https://jimmysong.io/kubernetes-handbook)
    1. Kubernetes权威指南第2版: [下载PDF](https://github.com/hbulpf/MLBooks/blob/master/6_%E4%BA%91%E8%AE%A1%E7%AE%97/Kubernetes%E6%9D%83%E5%A8%81%E6%8C%87%E5%8D%97%E7%AC%AC2%E7%89%88.pdf)
